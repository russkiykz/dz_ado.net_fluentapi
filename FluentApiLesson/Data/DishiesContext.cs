﻿using FluentApiLesson.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FluentApiLesson.Data
{
    public class DishiesContext:DbContext
    {
        public DishiesContext()
        {
            Database.Migrate();
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Dish> Dishes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-0V33MK0\\MSSQLSERVER01; Database = FluentApiLesson; Trusted_Connection=true;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                .ToTable("products")
                .Property(product => product.Id)
                .HasColumnName("ID");

            modelBuilder.Entity<Product>()
                .HasKey(product => product.Id);

            modelBuilder.Entity<Product>()
                .Property(product => product.Name)
                .HasColumnName("name")
                .HasMaxLength(50)
                .IsRequired();
            
            modelBuilder.Entity<Dish>()
                .ToTable("dishies")
                .Property(dish => dish.Id)
                .HasColumnName("ID");

            modelBuilder.Entity<Dish>()
                .HasKey(dish => dish.Id);

            modelBuilder.Entity<Dish>()
                .Property(dish => dish.Name)
                .HasColumnName("name")
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<DishProduct>()
                .ToTable("dishProducts")
                .Property(dishProduct => dishProduct.Id)
                .HasColumnName("ID");

            modelBuilder.Entity<DishProduct>()
                .HasKey(dishProduct=>dishProduct.Id);

            modelBuilder.Entity<DishProduct>()
                .Property(dish => dish.DishId)
                .HasColumnName("dishId");

            modelBuilder.Entity<DishProduct>()
                .Property(product => product.ProductId)
                .HasColumnName("productId");

            modelBuilder.Entity<DishProduct>()
                .HasOne(dishProduct => dishProduct.Dish).
                WithMany(dish => dish.DishProducts);

            modelBuilder.Entity<DishProduct>()
                .HasOne(dishProduct => dishProduct.Product).
                WithMany(product => product.DishProducts);


            base.OnModelCreating(modelBuilder);
        }
    }
}
