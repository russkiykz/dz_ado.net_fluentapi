﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FluentApiLesson.Migrations
{
    public partial class EditColumnName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_dishProducts_dishies_DishId",
                table: "dishProducts");

            migrationBuilder.DropForeignKey(
                name: "FK_dishProducts_products_ProductId",
                table: "dishProducts");

            migrationBuilder.RenameColumn(
                name: "ProductId",
                table: "dishProducts",
                newName: "productId");

            migrationBuilder.RenameColumn(
                name: "DishId",
                table: "dishProducts",
                newName: "dishId");

            migrationBuilder.RenameIndex(
                name: "IX_dishProducts_ProductId",
                table: "dishProducts",
                newName: "IX_dishProducts_productId");

            migrationBuilder.RenameIndex(
                name: "IX_dishProducts_DishId",
                table: "dishProducts",
                newName: "IX_dishProducts_dishId");

            migrationBuilder.AddForeignKey(
                name: "FK_dishProducts_dishies_dishId",
                table: "dishProducts",
                column: "dishId",
                principalTable: "dishies",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dishProducts_products_productId",
                table: "dishProducts",
                column: "productId",
                principalTable: "products",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_dishProducts_dishies_dishId",
                table: "dishProducts");

            migrationBuilder.DropForeignKey(
                name: "FK_dishProducts_products_productId",
                table: "dishProducts");

            migrationBuilder.RenameColumn(
                name: "productId",
                table: "dishProducts",
                newName: "ProductId");

            migrationBuilder.RenameColumn(
                name: "dishId",
                table: "dishProducts",
                newName: "DishId");

            migrationBuilder.RenameIndex(
                name: "IX_dishProducts_productId",
                table: "dishProducts",
                newName: "IX_dishProducts_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_dishProducts_dishId",
                table: "dishProducts",
                newName: "IX_dishProducts_DishId");

            migrationBuilder.AddForeignKey(
                name: "FK_dishProducts_dishies_DishId",
                table: "dishProducts",
                column: "DishId",
                principalTable: "dishies",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dishProducts_products_ProductId",
                table: "dishProducts",
                column: "ProductId",
                principalTable: "products",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
