﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FluentApiLesson.Migrations
{
    public partial class AddingLinks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_products_dishies_dishId",
                table: "products");

            migrationBuilder.DropIndex(
                name: "IX_products_dishId",
                table: "products");

            migrationBuilder.DropColumn(
                name: "dishId",
                table: "products");

            migrationBuilder.CreateTable(
                name: "dishProducts",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DishId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dishProducts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_dishProducts_dishies_DishId",
                        column: x => x.DishId,
                        principalTable: "dishies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dishProducts_products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_dishProducts_DishId",
                table: "dishProducts",
                column: "DishId");

            migrationBuilder.CreateIndex(
                name: "IX_dishProducts_ProductId",
                table: "dishProducts",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "dishProducts");

            migrationBuilder.AddColumn<Guid>(
                name: "dishId",
                table: "products",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_products_dishId",
                table: "products",
                column: "dishId");

            migrationBuilder.AddForeignKey(
                name: "FK_products_dishies_dishId",
                table: "products",
                column: "dishId",
                principalTable: "dishies",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
